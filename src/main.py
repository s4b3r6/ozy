print("Ozymandius v0.0.0")

try:
    import os
except OSError:
    print("Could not load OS library. Some functions might not work")

try:
    import gc
except OSError:
    print("Could not load GC library. Some functions might not work")

try:
    import network
except OSError:
    print("Could not load network library. Some functions might not work")

print("... Loaded.")

# TODO: Error Handling
# File eval
# File editor
# Piping
# Stdout, stderr seperation
# File redirection
# Sockets
# Web browser

global sysfile
sysfile = False

global vars
vars = {}

global ap_if
ap_if = network.WLAN(network.AP_IF)
global sta_if
sta_if = network.WLAN(network.STA_IF)

def iprint(*args, end='\n'):
    global sysfile
    if sysfile == False:
        print(*args, end=end)
    else:
        with open(sysfile, 'a+') as openFile:
            openFile.write(''.join(args))
            openFile.write(end)

def ozy_eval(com, args):
    if com == 'py':
        try:
            eval(' '.join(args))
        except:
            try:
                exec(' '.join(args))
            except Exception as e:
                iprint("Could not run command: {}".format(com))
                iprint("System Message: {}".format(e))
                import sys
                sys.print_exception(e)
                del sys
    elif com == 'batch':
        with open(args[0], 'r') as openFile:
            data = openFile.read()
        for line in data.split('\n'):
            if line:
                c2 = splitline(line)
                ozy_eval(c2[0], c2[1:])
    elif com == 'functions':
        funcs=[x for x in os.listdir("/std")]
        funcs.append("functions")
        funcs.append("batch")
        funcs.append("py")
        iprint(', '.join(funcs))
    else:
        try:
            f = open("/std/{}".format(com), 'r')
            exec("{}".format(f.read()), {'iprint':iprint,
                'sysfile': sysfile,
                'vars': vars,
                'ap_if':ap_if,
                'sta_if': sta_if,
                'os':os,
                'network':network}, {'args': args})
        except Exception as e:
            iprint("Could not run command: {}".format(com))
            iprint("System Message: {}".format(e))
            import sys
            sys.print_exception(e)
            del sys

def splitline(str):
    str = str + ' '
    instring = False
    token = ""
    tokens = []
    for c in str:
        #todo: escaping
        if instring:
            if c == '"':
                instring = False
                tokens.append(token)
                token = ''
            else:
                token = token + c
        else:
            if c == '"':
                instring = True
            elif c == ' ' or c == '\t' or c == '\n':
                if len(token) > 0:
                    tokens.append(token)
                    token = ''
            else:
                token = token + c
    return [x for x in tokens if x]

# Run startup file if it exists
try:
    f = open('/startup', 'r')
    f.close()
    ozy_eval('batch', {'/startup'})
except:
    pass

while True:
    com = splitline(input('RAM Free:{}\n{}> '.format(gc.mem_free(), os.getcwd())))
    if len(com) > 0:
        ozy_eval(com[0], com[1:])
    gc.collect()
