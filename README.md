# Ozymandius

An experimental platform for computing.

---

## Status: ***UNSTABLE***

There is next to no error handling, which could result in *significant* issues.

Memory use is currently interesting.

I have both an ESP-01 (blue, 1MB) and an ESP32 (2MB, no SpiRAM).

On average, the free memory is:

* ESP01, 25,000 bytes
* ESP32, 100,000 bytes

The difference is startling, and means that the ESP32 is fully capable of becoming a general purpose machine with little effort.

The ESP01 is *possible* to be turned into a general purpose machine, but does mean I'll probably need to rely on another chip to control the LCD/keyboard parts, (probably over UART), when building out the physical hardware.

---

## About

Ozymandius is designed to run atop of MicroPython on embedded hardware, particularly the ESP8266.

It may run in other environments, (and might not on the 512kb 'black' ESP-01), but I can't make any guarantees.

The intention is to supply an operating environment making it easier to use the device as a general computing device.

### Blog posts:

* [Ozymandius](https://jmilne.tk/jmilne#1529403597)
* [Cat: Memory Mouse](https://jmilne.tk/jmilne#1529472113)
* [Ozymandius - Dogfooding](https://jmilne.tk/jmilne#1529749712)
* [Ozymandius: Cooked](https://jmilne.tk/jmilne#1530059189)
